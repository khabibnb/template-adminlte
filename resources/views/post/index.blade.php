@extends('master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Bordered Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
         <div class="alert alert-success">
             {{session('success')}}
         </div>
        @endif
        <a class="btn btn-info mb-2" href="/pertanyaan/create">Tambah Pertanyaan</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Isi</th>
            <th style="width: 40px">Label</th>
          </tr>
        </thead>
        <tbody>
            @forelse($pertanyaan as $key=>$post)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$post->pertanyaan}}</td>
                    <td>{{$post->jawaban}}</td>
                    <td style="display: flex;">
                        <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">SHOW</a>
                        <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-warning btn-sm">EDIT</a>
                        <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" name="DELETE" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">NO POST</td>
                </tr>
            @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div>
  </div>
@endsection

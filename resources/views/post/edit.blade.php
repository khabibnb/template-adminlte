@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="/pertanyaan/{{$posts->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
              <div class="form-group">
                <label for="judul">Judul Pertanyaan</label>
                <input type="text" class="form-control" name="judul-pertanyaan" id="judul-pertanyaan" value="{{old('judul-pertanyaan', $posts->pertanyaan)}}" placeholder="Masukkan Judul">
              </div>
                <!-- textarea -->
              <div class="form-group">
                    <label>Isi Pertanyaan</label>
                      <input type="text" class="form-control" rows="3" name="isi-pertanyaan" id="isi-pertanyaan" value="{{old('isi-pertanyaan', $posts->jawaban)}}" placeholder="Isi Pertanyaan">
                    </div>
                  </div>
              </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card -->

    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
@endsection

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {Schema::table('pertanyaan', function (Blueprint $table) {
        $table->unsignedBigInteger('profil_id');
        $table->foreign('profil_id')->references('id')->on('profil');
        $table->unsignedBigInteger('jawaban_tepat_id');
        $table->foreign('jawaban_tepat_id')->references('id')->on('like_dislike_jawaban');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {Schema::table('posts', function (Blueprint $table) {
        //
    });
    }
}

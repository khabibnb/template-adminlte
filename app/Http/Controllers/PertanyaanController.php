<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create_pertanyaan()
    {
        return view('post.create_pertanyaan');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "pertanyaan"=>$request["judul-pertanyaan"],
            "jawaban"=> $request["isi-pertanyaan"],
            "tanggal_dibuat"=> "2000-01-01",
            "tanggal_diperbarui"=> "2000-01-01",
            "profil_id"=> NULL,
            "jawaban_tepat_id"=>NULL
        ]);
        return redirect('/pertanyaan')->with('success','Post Berhasil Disimpan');
    }
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('post.index', compact('pertanyaan'));
    }

    public function show($id)
    {
        $posts = DB::table('pertanyaan')->where('id',$id)->first();
        // dd($posts);
        return view('post.show', compact('posts'));
    }
    public function edit($id)
    {
        $posts = DB::table('pertanyaan')->where('id',$id)->first();
        // dd($posts);
        return view('post.edit', compact('posts'));
    }

    public function update($id, Request $request)
    {
        $query = DB::table('pertanyaan')
              ->where('id', $id)
              ->update([
                'pertanyaan' => $request['judul-pertanyaan'],
                'jawaban' => $request['isi-pertanyaan']
              ]);
        return redirect('/pertanyaan')->with('success', "Berhasil diupdate");
    }

    public function delete($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', "Berhasil didelete");
    }
}
